<?php
//If link url has "wc_license_key" then skip cart and go straight to checkout
if (isset($_GET["wc_license_key"])) {
	$_SESSION["wc_license_key"] = $_GET["wc_license_key"];
	add_filter ('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
	if(isset($_SERVER['HTTP_REFERER'])) {
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	function redirect_to_checkout() {
		$checkout_url = wc_get_checkout_url();
		return $checkout_url;
	}//end redirect_to_checkout
}// end if session
//Add DOB to Store Checkout
add_filter( 'woocommerce_checkout_fields' , 'slm_wooaddon_override_checkout_fields' );
global $current_user;
// Our hooked in function - $fields is passed via the filter!
function slm_wooaddon_override_checkout_fields( $fields ) {
	$fields['billing']['billing_license'] = array(
	'label'     => __('License', 'slm_woodaddon'),
	'type' => 'text',
	'placeholder'   => '',
	'required'  => false,
	'class'     => array('field-hidden'),
	'clear'     => true,
	'label_class' => array('field-hidden')
	 );
	if (isset($_SESSION["wc_license_key"])) {   
		$fields['billing']['billing_license']['default'] = ''.$_SESSION["wc_license_key"].'';
	}
	 return $fields;
}

function slm_wooaddon_license_notice() {
	global $woocommerce;
	$items = $woocommerce->cart->get_cart();

		global $_product_id, $_product_name, $post;
		$_product_id[] = array();
		$_variation_id[] = array();
		$_product_name[] = array();
		foreach($items as $item => $values) {
			global $_product_id, $_product_name, $_product; 
			$_product = wc_get_product( $values['data']->get_id());
			$_product_id[] = $values['product_id'];
			$_product_name[] = $_product->get_name();
			$_variation_id[] = $values['variation_id'];
			
		}//end foreach items
		if (isset($_SESSION["wc_license_key"])){
			global $wpdb, $LKarray, $LKey, $PID, $VID, $PN, $combos;
			$license_table = SLM_TBL_LICENSE_KEYS;
			$string= $_SESSION["wc_license_key"];
			$array = explode(',', $string);
			$array = implode("','",$array);
			$LKarray = explode(',', $string);
			$keyCheck = $wpdb->get_results("SELECT * FROM " . $license_table . " WHERE license_key IN ('$array')" );
			
			$LKey = array();
			$PID[] = array();
			$VID[] = array();
			$PN[] = array();
			$combos[] = array();
			foreach($keyCheck as $check){
				$LKey[] = $check->license_key;
				$PID[] = $check->product_id;
				$VID[] = $check->variation_id;
				$PN[] = $check->product_name;
				
				if($check->variation_attribute == true){
					$va = ' - '.$check->variation_attribute;	
				}else{
					$va ='';	
				}
				
				$combos[] = $check->product_name.$va.' - '.$check->license_key;
			}
			
			if($LKarray == $LKey && $_product_name == $PN && $_product_id == $PID && $_variation_id == $VID){
				$validate = 'true';
			}else{
				
			}//end check if license key's, product id's and product name's arrays match.
			
			if($validate != 'true'){
				?>
                	<div style="background:#FFF; position:fixed; z-index:99; max-width:500px;
                    height:auto; border:2px solid #000; padding:10px; top: 50%; left: 50%; transform: translate(-50%, -50%);" id="key-warning">
                    	
                        	<p>One or more license keys were not valid for your product(s), please check your licenses section in your account to verify license number and try again. <?php echo $_SESSION["wc_license_key"]; ?></p>
                        	<a href="#" class="edd-submit button" onClick="location.href='?cancel_license_renewal'"><?php _e('CLOSE','slm_wooaddon') ?></a>
                        </form>
                    </div>
                <?php
			}else{
				
			}
			
		}//end if $_SESSION key;
	?>
	<div class="renewal_notice">
		<p>
			<?php _e('Renewing a license key?','slm_wooaddon') ?> <a class="renew-link" href="#" id="renewLink" ><?php _e('Click to renew any existing licenses','slm_wooaddon') ?></a>
			<?php if (isset($_SESSION["wc_license_key"])){?>
                <br><strong><?php _e('License Key(s) being renewed','slm_wooaddon') ?>:</strong>
                <br><?php 
					foreach(array_slice($combos,1) as $key=> $combo){
						echo $combo.'<br>';
					}
				
				} ?>
		</p>
		<p class="license-key-container-wrap">
		<span class="edd-description"><?php _e('Enter the license key you wish to renew. Leave blank to purchase a new one.','slm_wooaddon') ?><br><i style="font-size:11px;">If you have more than one product with a license key that you are renewing, then separate each key with a comma.</i></span>
<input id="license-key" class="license-input" type="text" value="" placeholder="<?php _e('Enter your license key','slm_wooaddon') ?>" autocomplete="off" name="license_key">
		
			<a href="#" class="edd-submit button blue" onclick="location.href='?wc_license_key='+document.getElementById('license-key').value;"><?php _e('APPLY LICENSE RENEWAL','slm_wooaddon') ?></a>
			<span>
				<a id="edd-cancel-license-renewal" href="#"><?php _e('Cancel','slm_wooaddon') ?></a>
			</span>
		</p>
	</div><!-- end of: renewal notice -->
	<?php
	if (isset($_SESSION["wc_license_key"])){
	?>
		<div id="edd_sl_cancel_renewal">
			<p>
				<a href="#" class="edd-submit button" onClick="location.href='?cancel_license_renewal'"><?php _e('Cancel License Renewal','slm_wooaddon') ?></a>
			</p>
		</div><!-- end of: cancel renewal -->
	<?php
	} ?>
		<script>
		jQuery('.edd-submit').click(function(event) {
			event.preventDefault();
		});
		jQuery('#renewLink').click(function(event) {
			event.preventDefault();
			jQuery('#renewLink').hide();
			jQuery('.license-key-container-wrap').show();
		});
		jQuery('#edd-cancel-license-renewal').click(function(event) {
			event.preventDefault();
			jQuery('#renewLink').show();
			jQuery('.license-key-container-wrap').hide();
		});
	</script>
	<?php
	if(isset($_GET['cancel_license_renewal'])){
		unset($_SESSION["wc_license_key"]);
		header('Location: ' . wc_get_checkout_url());
	}
}

add_action( 'woocommerce_checkout_before_order_review', 'slm_wooaddon_license_notice' );

?>