<?php
/**
 * Auto Complete all WooCommerce orders. Uncomment this function for test purposes only. This will allow orders to be autocompleted and make sure that paypal sandbox or check orders don't get in a hold status and no license key created.

add_action( 'woocommerce_thankyou', 'slm_wooaddon_woocommerce_auto_complete_order' );
function slm_wooaddon_woocommerce_auto_complete_order( $order_id ) { 
	if ( ! $order_id ) {
		return;
	}

	$order = wc_get_order( $order_id );
	$order->update_status( 'completed' );
}//end slm_wooaddon_woocommerce_auto_complete_order
*/ 

/*** Woocommerce completed order hook ***/
//This hook/funcion fires only when a order is officially created and payment has been verified. Paypal sandbox doesn't not verify payment back to the site. So, uncommenting the function above will help in testing. License keys will only be created when a real payment has been verified.
function slm_wooaddon_woocommerce_order_status_completed( $order_id ) {

	global $woocommerce, $post, $user_id;
	
	$order = new WC_Order($order_id);
	
	//to escape # from order id 
	$order_id = trim(str_replace('#', '', $order->get_order_number()));
	
	$items = $order->get_items();
	$licenseKey = false;
	$is_licensed = false;
	foreach ( $items as $item ) {
		 $product = $order->get_product_from_item( $item );
		 $licenseKey = $licenseKey || get_post_meta($product->id, '_product_license', true);
		 $product_id = $product->id;
		 $variation_id = $product->variation_id;
		 $is_licensed = $is_licensed || get_post_meta($variation_id, '_variable_is_product_license', true);
	}//end foreach items
	
	if(!isset($_SESSION["wc_license_key"]) && $licenseKey || !isset($_SESSION["wc_license_key"]) && $is_licensed ){

		//define('YOUR_LICENSE_SERVER_URL', home_url()); //Rename this constant name so it is specific to your plugin or theme.
		
		/*** Mandatory data ***/
		$items = $order->get_items();
		foreach ( $items as $item ) {
			//$product = $order->get_product_from_item( $item ); // This became deprecated
			$product = $item->get_product();
			$licenseKey = get_post_meta($product->id, '_product_license', true);
			$is_licensed = get_post_meta($product->variation_id, '_variable_is_product_license', true);
			if(($licenseKey || $is_licensed ) && ($product->is_type('simple')|| $product->is_type('variation'))) {
				$product_name = $item['name'];
				$product_id = $item['product_id'];
				$variation_id = $item['variation_id'];
				$subscription_options = $item['subscription-options'];
				$firstname = $order->billing_first_name;
				$lastname = $order->billing_last_name;
				$email = $order->billing_email;
				$company = $order->billing_company;
				$date = date('Y-m-d');
				$dateExpiry = date('Y-m-d', strtotime('+1 year'));
				$product_license_count = get_post_meta($variation_id, '_variable_is_product_license_count', true);
				 if($product_license_count){
						$license_count  = $product_license_count;
				 }else{
					 $product_license_count = get_post_meta($variation_id, '_variable_is_product_license_count', true);
					 if ($product_license_count){
						 $license_count  = $product_license_count;
					 }
					 else{
						$license_count = '1';
					 }
				 }

				// retrieve slm key from options
				global $slm_debug_logger;
				global $wpdb;

				$options = get_option('slm_plugin_options');

				$lic_key_prefix = $options['lic_prefix'];

				$fields = array();
				$fields['license_key'] = uniqid($lic_key_prefix);
				$fields['lic_status'] = 'pending';
				$fields['first_name'] = $firstname;
				$fields['last_name'] = $lastname;
				$fields['email'] = $email;
				$fields['company_name'] = $company;
				$fields['txn_id'] = $order_id;
				$fields['product_id'] = $product_id;
				$fields['variation_id'] = $variation_id;
				$fields['variation_attribute'] = $subscription_options;
				$fields['product_name'] = $product_name;
				$fields['date_created'] = $date;//Today's date
				$fields['date_expiry'] = $dateExpiry;//Today's date
				$fields['max_allowed_domains'] = $license_count; //TODO - later take from estore's product configuration



				$slm_debug_logger->log_debug('Inserting license data into the license manager DB table.');

				$fields = array_filter($fields);//Remove any null values.


				$tbl_name = SLM_TBL_LICENSE_KEYS;

				$result = $wpdb->insert($tbl_name, $fields);

				if(!$result){

					$slm_debug_logger->log_debug('Notice! initial database table insert failed on license key table (User Email: '.$fields['email'].'). Trying again by converting charset', true);
				}
			}
		}//end foreach item
		
	}//If License Key is not set

	//If License Key is set
	if (isset($_SESSION["wc_license_key"])) {
		global $wpdb, $woocommerce, $post, $user_id;
	
		$tbl_name = SLM_TBL_LICENSE_KEYS;
	
		$reg_table = SLM_TBL_LIC_DOMAIN;
		
		$expireDate = date('Y-m-d', strtotime('+1 year'));
		$currentDate = date('Y-m-d');
	
		$order = new WC_Order($order_id);
	
		//to escape # from order id 
		$order_id = trim(str_replace('#', '', $order->get_order_number()));
	
		//$key = $_SESSION["wc_license_key"];
		
		$string= $_SESSION["wc_license_key"];
		$array = explode(',', $string);
		$array = implode("','",$array);

		$keyCheck = $wpdb->get_results("SELECT * FROM " . $tbl_name . " WHERE license_key IN ('$array')" );
		
		foreach($keyCheck as $check){
			$key = $check->license_key;
	
			$sql_prep1 = $wpdb->prepare("SELECT * FROM $tbl_name WHERE license_key = %s", $key);
		
			$retLic = $wpdb->get_row($sql_prep1, OBJECT);
			
			$data = array('lic_status' => 'active', 'txn_id' => $order_id, 'date_renewed' => $currentDate, 'date_expiry' => $expireDate);
			
			$where = array('id' => $retLic->id);
		
			$updated = $wpdb->update($tbl_name, $data, $where);
		}//end foreach keyCheck
		
	}//end if license key set

}//end order status completed

add_action( 'woocommerce_order_status_completed', 'slm_wooaddon_woocommerce_order_status_completed' );
?>