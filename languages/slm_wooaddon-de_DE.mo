��          �   %   �      p     q  (   �     �     �  *   �  "   �  #     6   @  K   w     �     �     �  S   �     F     K     S     _     z     �     �     �     �     �     �     �  >   �  >   "  v  a     �  '   �  	          >   7  2   v  #   �  =   �  b   	     n	     �	     �	  r   �	     
     '
     .
     ?
     ^
  
   g
     r
     z
     �
     �
     �
     �
  I   �
  0                                                                       	                                                   
    APPLY LICENSE RENEWAL Action Needed: subscription expires soon Cancel Cancel License Renewal Click %s to expand and see active domains. Click to renew an existing license Does this product include a license Enter the amount of licenses this item price will have Enter the license key you wish to renew. Leave blank to purchase a new one. Enter your license key Expire Date Hi there %s If this downloadable product will have a license key, then check the following box. Item License License Key License Keys being renewed Licenses Order Product Product License Product License Count Renew License Renewing a license key? Sites We noticed that one of your %s subscriptions will expire soon: You currently do not have any plugins purchased with licenses. Project-Id-Version: SLM WooCoomerce Addon
POT-Creation-Date: 2016-08-06 12:08+0100
PO-Revision-Date: 2016-08-06 12:14+0100
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.3
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 LIZENZ ERNEURN Aktion benötigt: Abonnement endet bald Abbrechen Abbrechen Lizenzverlängerung Klicken Sie auf %s zu erweitern und aktiven Domänen zu sehen. Klicken Sie auf eine bestehende Lizenz zu erneuern Enthält dieses Produkt eine Lizenz Geben Sie den Anzahl von Lizenzen dieser Artikelpreis bekommt Geben Sie den Lizenzschlüssel, den Sie verlängern möchten. Freilassen um einen neuen zu kaufen. Geben Sie Ihre Lizenzschlüssel Verfalldatum Hallo %s Wenn dieses Produkt zum Herunterladen einen Lizenzschlüssel haben müss, dann überprüfen Sie die folgende Feld. Produkt/Artikel Lizenz Lizenzschlüssel Lizenzschlüssel wird erneuert Lizenzen BesteIlung Produkt Produktlizenz Produktlizenzanzahl Lizenz erneuern Lizenzschlüssel verlängern? Seiten Wir haben festgestellt, dass einer Ihrer %s Abonnemente in Kürze läuft: Sie haben derzeit keine Plugin Lizenzen gekauft. 