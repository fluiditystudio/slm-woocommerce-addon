<?php
/*
Plugin Name: SLM WooCoomerce Addon
Plugin URI: http://fluiditystudio.com/wppplugins/
Description: This is a WooCommerce integration for the "Software License Manger" plugin. This addon will allow for assigning license keys to digital products at checkout, as well as renewing product licenses.
Version: 3.5
Author: Tyler Robinson
Author URI: http://fluiditystudio.com/
License: GPL2
Text Domain: slm_wooaddon
WC requires at least: 3.0.0
WC tested up to: 5.8.1
*/

// Load the auto-update class
add_action( 'init', 'slm_wooaddon_activate' );
function slm_wooaddon_activate()
{
	require_once ( 'wp_autoupdate.php' );
	$plugin_current_version = '3.5';
	$plugin_remote_path = 'http://fluiditystudio.com/wpplugins/slm-woocommerce-addon/update.php';
	$plugin_slug = plugin_basename( __FILE__ );
	$license_user = '';
	$license_key = '';
	new WP_AutoUpdate ( $plugin_current_version, $plugin_remote_path, $plugin_slug, $license_user, $license_key );
}

//Activation hook and run functions
function slm_woocommerce_addon_install()
{
    global $wpdb;

	//Add to field to license key table if doesn't exist.
	$lic_key_table = SLM_TBL_LICENSE_KEYS;

	$row = $wpdb->get_results("SELECT * FROM {$lic_key_table}");

	if(!isset($row->product_id)){
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN product_id varchar(64) NOT NULL after txn_id");
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN variation_id varchar(64) NOT NULL after product_id");
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN product_name varchar(64) NOT NULL after variation_id");
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN variation_attribute varchar(64) NOT NULL after product_name");
	}
}
register_activation_hook(__FILE__, 'slm_woocommerce_addon_install');

//Add menu item
function add_slm_wooaddon_menu_item()
{
	add_menu_page("SLM Woo Addon", "SLM Woo Addon", "manage_options", "slm-wooaddon-panel", "slm_wooaddon_settings_page", plugins_url( 'images/icon.png', __FILE__ ), null, 99);
	load_plugin_textdomain( 'slm_wooaddon', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

add_action("admin_menu", "add_slm_wooaddon_menu_item");

function slm_wooaddon_settings_page(){
	?>
	<h1>SLM Woocommerce Addon</h1>
	<h2>Instructions for the Software License Manager WooCommerce Addon.</h2>
	
	<h3>Features:</h3>
	<p>This plugin will integrate the "Software License Manager" plugin with your WooCommerce Store.</p>
	<ol>
		<li>Product license checkbox now added for "Simple" and "Variable" products.</li>
		<li>Variable products also have a domain limit field allowing you to sell one or more site license plugins.</li>
		<li>Upon payment completion a license key will be created if the "Product License" checkbox is checked. An email with their order details/download link and license key is sent.</li>
		<li>At checkout the user may also enter their license key to renew a plugin they already have.</li>
		<li>Renewal email reminders will automatically be sent out at 1 month, 2 weeks &amp; 3 days before expiration date. This renewal email will have their product name and expire date along with a button/link to the checkout page which will then pre populate the checkout with their product and license key. Upon payment for renewal, their license expire date will be updated a year from that day.</li>
		<li>Customer account page now has a "License" section which shows the order number and link to the order review, as well as the product name, license key and expire date. Clicking the "+" symbol will expand to show domain(s) activated for that license key.</li>
	</ol>
	
	<h3>Instructions:</h3>
	<p>To have the system run a check for license expiration dates and send out emails you will need to install a plugin called "Cronjob Scheduler" by chrispage1, which you can find in the Wordpress Plugin Directory. Once installed, you will see a link to this under "Settings" called "Cronjob Scheduler". Follow the instructions and then reload the page. You will then see a place to add a Cron Action at the bottom.</p>
	<p>Enter the following function call "check_license_expire_date" and select how often you want the system to run it. Then click "Create Cronjob" and your all set!</p>
	
	<p>The renewal email that goes out is very generic with a welcome note about a license that is soon to expire, along with a table showing the product and expiration date. Below that is a button with a link to the shopping cart to renew their key.</p>
	
	<p>If you would like to add a header and footer into this email template the go into the plugin folder "slm-woocommerce-addon" and copy the renew-email-template.php file from the "templates" folder and create a folder in your theme called "slm-addon" and place it in there. You can then add header and footer content as well as change/add some verbiage.</p>
	
	<h3>Renewal Email Example Image</h3>
	<img src="<?php echo plugins_url('images/renewal_email_example.png', __FILE__ ); ?>" />
	
	<h3>UPDATE</h3>
	
	<p>In order to now use variable products with different site license amounts, you must create an Attribute called "Subscription Options". In that Attribute, you can add terms like "Single site license ($50)" and "5 Site license ($100)".</p>
	
	<h3>Video Walkthrough</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/ezKe9msEuGg" frameborder="0" allowfullscreen></iframe>
	
	<h3>Video Walkthrough Update</h3>
	<iframe src="https://www.youtube.com/embed/VYi0_msPj1w" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
	
	<?php
}

//WooCommerce link directly to checkout
include_once('woocommerce-link-to-checkout.php');

//WooCommerce add "License" option to product.
include_once('woocommerce-license-checkbox.php');
 
//WooCommerce add license endpoint to My Account page
include_once('woocommerce-license-endpoint.php');
 
//WooCommerce on completed order issue plugin license key
include_once('woocommerce-on-completed-order.php');

//WooCommerce email befor order table hook
include_once('woocommerce-email-hook.php');

//WooCommerce check license expire date
include_once('check-license-expire-date.php');

//Add custom fields to add/edit license manager
include_once('slm-add-fields-to-license-manager.php');

//WooCommerce multiple add to cart products in link
include_once('woocommerce-add-multiple-to-cart-link.php');

function slm_wooaddon_custom_scripts(){

	wp_register_style( 'sflmWoo',  plugins_url('css/styles.css', __FILE__ ), false, true);
	wp_enqueue_style( 'sflmWoo' );
	
	wp_enqueue_style('FontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', '4.7.0', "all"); 
}
add_action('wp_enqueue_scripts', 'slm_wooaddon_custom_scripts');

?>