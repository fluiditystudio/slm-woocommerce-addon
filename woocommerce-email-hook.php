<?php
add_action( 'woocommerce_email_after_order_table', 'slm_wooaddon_add_product_license_to_email', 10, 2 );
 
function slm_wooaddon_add_product_license_to_email( $order, $sent_to_admin ) {
  
	global $wpdb;
	$orderNumber = $order->get_order_number();
	$license_table = SLM_TBL_LICENSE_KEYS;
	$data = $wpdb->get_results('SELECT * FROM ' . $license_table . ' WHERE txn_id = "'.$orderNumber.'"');
	foreach($data as $dat){
		$license_key = $dat->license_key;
		$product_name = $dat->product_name;
		$max_domains = $dat->max_allowed_domains;
		if($license_key){
			?>
			<div style="width:100%;margin-top:7px;border:1px solid #e1e1e1; padding:12px;">
			Your License for <?php echo $product_name; ?> Key is <?php echo $license_key; ?> with a maximum of <?php echo $max_domains; ?> sites allowed.
			</div>
			<?php		
		}
	}
}
?>